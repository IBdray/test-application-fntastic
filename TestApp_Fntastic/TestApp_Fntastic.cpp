// TestApp_Fntastic.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <unordered_map>

// Forward declaration
std::string TransformToBrackets(const std::string& String);
bool IsUpperCase(const char Letter);
char ToLowerCase(const char Letter);


std::string TransformToBrackets(const std::string& String)
{
	std::unordered_map<char, int> DuplicatesMap;
	std::string Result;

	for (const char Letter : String)
		DuplicatesMap[ToLowerCase(Letter)] += 1;

	for (const char Letter : String)
		Result += (DuplicatesMap[(ToLowerCase(Letter))] > 1) ? ')' : '(';

	return Result;
}

char ToLowerCase(const char Letter)
{
	const int DifferenceBetweenLowerAndUpperCases = 32;

	if (IsUpperCase(Letter))
		return Letter + DifferenceBetweenLowerAndUpperCases;
	return Letter;
}

bool IsUpperCase(const char Letter)
{
	const int AskiiTableACode = 65;
	const int AskiiTableZCode = 90;

	return Letter >= AskiiTableACode && Letter <= AskiiTableZCode;
}


int main()
{
	std::cout << "This app will transform your input strings to bracket strings." << "\n";
	std::cout << "If a character occurs more than once in string, it will be converted to \")\"" << "\n";
	std::cout << "and if it occurs only once, it will be converted to \"(\"" << "\n";
	std::cout << "Case insensitive." << "\n" << "\n";

	std::cout << "You can input multiple strings." << "\n" << "\n";


	while (true)
	{
		std::cout << "Please enter your string:" << "\n";

		std::string InputText;
		std::cin >> InputText;

		std::cout << TransformToBrackets(InputText) << " - Your transformed string!" << "\n" << "\n";
	}
}
