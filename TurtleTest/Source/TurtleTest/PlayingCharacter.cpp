// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayingCharacter.h"

#include "InteractableActor.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/KismetSystemLibrary.h"


// Sets default values
APlayingCharacter::APlayingCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	InteractionDetectionLength = 200.0f;

	Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	Scene->SetupAttachment(RootComponent);
	
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetupAttachment(GetMesh());
	Camera->bUsePawnControlRotation = true;

	// Lift camera to eyes level
	Camera->SetRelativeLocation(FVector(0.0f,0.0f,GetCapsuleComponent()->GetScaledCapsuleHalfHeight()));
}

// Called when the game starts or when spawned
void APlayingCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APlayingCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	DetectInteractableActors();
}

void APlayingCharacter::DetectInteractableActors()
{
	const FHitResult LineTraceResult = LineTraceForInteractableActors();
	
	if (LineTraceResult.IsValidBlockingHit())
		SetInteractionTargetIfValid(LineTraceResult);
	else
		ResetInteractionTarget();
}

FHitResult APlayingCharacter::LineTraceForInteractableActors() const
{
	FHitResult HitResult;
	UKismetSystemLibrary::LineTraceSingle(GetWorld(), Camera->GetComponentLocation(),
		CalculateDetectionEndVector(), TraceTypeQuery1, false, NonInteractableActors,
		EDrawDebugTrace::None, HitResult, true);

	return HitResult;
}

FVector APlayingCharacter::CalculateDetectionEndVector() const
{
	return Camera->GetForwardVector() * InteractionDetectionLength + Camera->GetComponentLocation();
}

void APlayingCharacter::SetInteractionTargetIfValid(const FHitResult& HitResult)
{
	if (HitResult.GetActor())
	{
		const auto TempInteractable = Cast<AInteractableActor>(HitResult.GetActor());
	
		if (TempInteractable && TempInteractable != InteractionTarget)
		{
			InteractionTarget = TempInteractable;
			OnInteractableFound();
		}
	}
}

void APlayingCharacter::ResetInteractionTarget()
{
	InteractionTarget = nullptr;
	OnInteractableLost();
}

// Called to bind functionality to input
void APlayingCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &APlayingCharacter::InteractWithTarget);

	PlayerInputComponent->BindAxis("MoveForward", this, &APlayingCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &APlayingCharacter::MoveRight);
	PlayerInputComponent->BindAxis("LookUp", this, &APlayingCharacter::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("Turn", this, &APlayingCharacter::AddControllerYawInput);
}

void APlayingCharacter::InteractWithTarget()
{
	if (InteractionTarget)
		InteractionTarget->Interact();
}

void APlayingCharacter::MoveForward(float AxisValue)
{
	if (Controller != nullptr && AxisValue != 0.0)
		AddMovementInput(GetActorForwardVector(), AxisValue);
	OnFootstep();
}
void APlayingCharacter::MoveRight(float AxisValue)
{
	if (Controller != nullptr && AxisValue != 0.0)
		AddMovementInput(GetActorRightVector(), AxisValue);
	OnFootstep();
}

void APlayingCharacter::AddControllerPitchInput(float AxisValue)
{
	Super::AddControllerPitchInput(AxisValue);
}
void APlayingCharacter::AddControllerYawInput(float AxisValue)
{
	Super::AddControllerYawInput(AxisValue);
}

