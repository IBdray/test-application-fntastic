// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PlayingCharacter.generated.h"

class UCameraComponent;
class USceneComponent;
class AInteractableActor;

UCLASS()
class TURTLETEST_API APlayingCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayingCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	virtual void AddControllerPitchInput(float AxisValue) override;
	virtual void AddControllerYawInput(float AxisValue) override;
	void MoveForward(float AxisValue);
	void MoveRight(float AxisValue);

	void DetectInteractableActors();
	FHitResult LineTraceForInteractableActors() const;
	FVector CalculateDetectionEndVector() const;
	void SetInteractionTargetIfValid(const FHitResult& HitResult);
	void ResetInteractionTarget();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Interaction")
	float InteractionDetectionLength;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Components")
	USceneComponent* Scene;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Components")
	UCameraComponent* Camera;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Interaction")
	TArray<AActor*> NonInteractableActors;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Interaction")
	AInteractableActor* InteractionTarget;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable)
	void InteractWithTarget();
	
	UFUNCTION(BlueprintImplementableEvent)
	void OnFootstep();
	UFUNCTION(BlueprintImplementableEvent)
	void OnInteractableFound();
	UFUNCTION(BlueprintImplementableEvent)
	void OnInteractableLost();
};
