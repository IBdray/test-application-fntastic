// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InteractableActor.generated.h"

class USceneComponent;

UCLASS()
class TURTLETEST_API AInteractableActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AInteractableActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
	USceneComponent* Scene;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Interactable States")
	bool bCanBeActivated;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Interactable States")
	bool bActivated;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	bool CanBeActivated() const;
	UFUNCTION(BlueprintCallable)
	bool IsActivated() const;

	UFUNCTION(BlueprintImplementableEvent)
	void Interact();
};
