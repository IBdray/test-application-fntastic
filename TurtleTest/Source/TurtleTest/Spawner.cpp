// Fill out your copyright notice in the Description page of Project Settings.


#include "Spawner.h"

#include "Components/ArrowComponent.h"
#include "GameFramework/Character.h"

// Sets default values
ASpawner::ASpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SpawnableCharacter = ACharacter::StaticClass();

	Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	Scene->SetupAttachment(RootComponent);

	SpawnPosition = CreateDefaultSubobject<UArrowComponent>(TEXT("Spawn Position"));
	SpawnPosition->SetupAttachment(Scene);
}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

ACharacter* ASpawner::SpawnCharacter()
{
	ACharacter* Character = GetWorld()->SpawnActor<ACharacter>(SpawnableCharacter, SpawnPosition->GetComponentLocation(),
		SpawnPosition->GetComponentRotation());
	
	SpawnedCharacters.Emplace(Character);
	OnSpawnCharacter(Character);
	return Character;
}

