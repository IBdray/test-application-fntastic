// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TurtleTestGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TURTLETEST_API ATurtleTestGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
