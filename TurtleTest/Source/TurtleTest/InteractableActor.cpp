// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractableActor.h"

#include "Components/SceneComponent.h"

// Sets default values
AInteractableActor::AInteractableActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bCanBeActivated = true;
	
	Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	Scene->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AInteractableActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AInteractableActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

bool AInteractableActor::CanBeActivated() const
{
	return bCanBeActivated;
}

bool AInteractableActor::IsActivated() const
{
	return bActivated;
}

