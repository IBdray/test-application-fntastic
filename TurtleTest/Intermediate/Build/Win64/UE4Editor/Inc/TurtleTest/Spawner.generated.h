// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ACharacter;
#ifdef TURTLETEST_Spawner_generated_h
#error "Spawner.generated.h already included, missing '#pragma once' in Spawner.h"
#endif
#define TURTLETEST_Spawner_generated_h

#define TurtleTest_Source_TurtleTest_Spawner_h_16_SPARSE_DATA
#define TurtleTest_Source_TurtleTest_Spawner_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSpawnCharacter);


#define TurtleTest_Source_TurtleTest_Spawner_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSpawnCharacter);


#define TurtleTest_Source_TurtleTest_Spawner_h_16_EVENT_PARMS \
	struct Spawner_eventOnSpawnCharacter_Parms \
	{ \
		ACharacter* Character; \
	};


#define TurtleTest_Source_TurtleTest_Spawner_h_16_CALLBACK_WRAPPERS
#define TurtleTest_Source_TurtleTest_Spawner_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASpawner(); \
	friend struct Z_Construct_UClass_ASpawner_Statics; \
public: \
	DECLARE_CLASS(ASpawner, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TurtleTest"), NO_API) \
	DECLARE_SERIALIZER(ASpawner)


#define TurtleTest_Source_TurtleTest_Spawner_h_16_INCLASS \
private: \
	static void StaticRegisterNativesASpawner(); \
	friend struct Z_Construct_UClass_ASpawner_Statics; \
public: \
	DECLARE_CLASS(ASpawner, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TurtleTest"), NO_API) \
	DECLARE_SERIALIZER(ASpawner)


#define TurtleTest_Source_TurtleTest_Spawner_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASpawner(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASpawner) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawner); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawner); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawner(ASpawner&&); \
	NO_API ASpawner(const ASpawner&); \
public:


#define TurtleTest_Source_TurtleTest_Spawner_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawner(ASpawner&&); \
	NO_API ASpawner(const ASpawner&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawner); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawner); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASpawner)


#define TurtleTest_Source_TurtleTest_Spawner_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Scene() { return STRUCT_OFFSET(ASpawner, Scene); } \
	FORCEINLINE static uint32 __PPO__SpawnPosition() { return STRUCT_OFFSET(ASpawner, SpawnPosition); } \
	FORCEINLINE static uint32 __PPO__SpawnableCharacter() { return STRUCT_OFFSET(ASpawner, SpawnableCharacter); } \
	FORCEINLINE static uint32 __PPO__SpawnedCharacters() { return STRUCT_OFFSET(ASpawner, SpawnedCharacters); }


#define TurtleTest_Source_TurtleTest_Spawner_h_13_PROLOG \
	TurtleTest_Source_TurtleTest_Spawner_h_16_EVENT_PARMS


#define TurtleTest_Source_TurtleTest_Spawner_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TurtleTest_Source_TurtleTest_Spawner_h_16_PRIVATE_PROPERTY_OFFSET \
	TurtleTest_Source_TurtleTest_Spawner_h_16_SPARSE_DATA \
	TurtleTest_Source_TurtleTest_Spawner_h_16_RPC_WRAPPERS \
	TurtleTest_Source_TurtleTest_Spawner_h_16_CALLBACK_WRAPPERS \
	TurtleTest_Source_TurtleTest_Spawner_h_16_INCLASS \
	TurtleTest_Source_TurtleTest_Spawner_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TurtleTest_Source_TurtleTest_Spawner_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TurtleTest_Source_TurtleTest_Spawner_h_16_PRIVATE_PROPERTY_OFFSET \
	TurtleTest_Source_TurtleTest_Spawner_h_16_SPARSE_DATA \
	TurtleTest_Source_TurtleTest_Spawner_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	TurtleTest_Source_TurtleTest_Spawner_h_16_CALLBACK_WRAPPERS \
	TurtleTest_Source_TurtleTest_Spawner_h_16_INCLASS_NO_PURE_DECLS \
	TurtleTest_Source_TurtleTest_Spawner_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TURTLETEST_API UClass* StaticClass<class ASpawner>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TurtleTest_Source_TurtleTest_Spawner_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
