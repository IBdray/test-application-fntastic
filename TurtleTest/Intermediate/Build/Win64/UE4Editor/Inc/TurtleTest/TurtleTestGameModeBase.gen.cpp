// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TurtleTest/TurtleTestGameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTurtleTestGameModeBase() {}
// Cross Module References
	TURTLETEST_API UClass* Z_Construct_UClass_ATurtleTestGameModeBase_NoRegister();
	TURTLETEST_API UClass* Z_Construct_UClass_ATurtleTestGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_TurtleTest();
// End Cross Module References
	void ATurtleTestGameModeBase::StaticRegisterNativesATurtleTestGameModeBase()
	{
	}
	UClass* Z_Construct_UClass_ATurtleTestGameModeBase_NoRegister()
	{
		return ATurtleTestGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_ATurtleTestGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATurtleTestGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_TurtleTest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATurtleTestGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "TurtleTestGameModeBase.h" },
		{ "ModuleRelativePath", "TurtleTestGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATurtleTestGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATurtleTestGameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ATurtleTestGameModeBase_Statics::ClassParams = {
		&ATurtleTestGameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_ATurtleTestGameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ATurtleTestGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATurtleTestGameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ATurtleTestGameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ATurtleTestGameModeBase, 2765099770);
	template<> TURTLETEST_API UClass* StaticClass<ATurtleTestGameModeBase>()
	{
		return ATurtleTestGameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ATurtleTestGameModeBase(Z_Construct_UClass_ATurtleTestGameModeBase, &ATurtleTestGameModeBase::StaticClass, TEXT("/Script/TurtleTest"), TEXT("ATurtleTestGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATurtleTestGameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
