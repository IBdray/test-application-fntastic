// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TURTLETEST_TurtleTestGameModeBase_generated_h
#error "TurtleTestGameModeBase.generated.h already included, missing '#pragma once' in TurtleTestGameModeBase.h"
#endif
#define TURTLETEST_TurtleTestGameModeBase_generated_h

#define TurtleTest_Source_TurtleTest_TurtleTestGameModeBase_h_15_SPARSE_DATA
#define TurtleTest_Source_TurtleTest_TurtleTestGameModeBase_h_15_RPC_WRAPPERS
#define TurtleTest_Source_TurtleTest_TurtleTestGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define TurtleTest_Source_TurtleTest_TurtleTestGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATurtleTestGameModeBase(); \
	friend struct Z_Construct_UClass_ATurtleTestGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ATurtleTestGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/TurtleTest"), NO_API) \
	DECLARE_SERIALIZER(ATurtleTestGameModeBase)


#define TurtleTest_Source_TurtleTest_TurtleTestGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesATurtleTestGameModeBase(); \
	friend struct Z_Construct_UClass_ATurtleTestGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ATurtleTestGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/TurtleTest"), NO_API) \
	DECLARE_SERIALIZER(ATurtleTestGameModeBase)


#define TurtleTest_Source_TurtleTest_TurtleTestGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATurtleTestGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATurtleTestGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATurtleTestGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATurtleTestGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATurtleTestGameModeBase(ATurtleTestGameModeBase&&); \
	NO_API ATurtleTestGameModeBase(const ATurtleTestGameModeBase&); \
public:


#define TurtleTest_Source_TurtleTest_TurtleTestGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATurtleTestGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATurtleTestGameModeBase(ATurtleTestGameModeBase&&); \
	NO_API ATurtleTestGameModeBase(const ATurtleTestGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATurtleTestGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATurtleTestGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATurtleTestGameModeBase)


#define TurtleTest_Source_TurtleTest_TurtleTestGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define TurtleTest_Source_TurtleTest_TurtleTestGameModeBase_h_12_PROLOG
#define TurtleTest_Source_TurtleTest_TurtleTestGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TurtleTest_Source_TurtleTest_TurtleTestGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	TurtleTest_Source_TurtleTest_TurtleTestGameModeBase_h_15_SPARSE_DATA \
	TurtleTest_Source_TurtleTest_TurtleTestGameModeBase_h_15_RPC_WRAPPERS \
	TurtleTest_Source_TurtleTest_TurtleTestGameModeBase_h_15_INCLASS \
	TurtleTest_Source_TurtleTest_TurtleTestGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TurtleTest_Source_TurtleTest_TurtleTestGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TurtleTest_Source_TurtleTest_TurtleTestGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	TurtleTest_Source_TurtleTest_TurtleTestGameModeBase_h_15_SPARSE_DATA \
	TurtleTest_Source_TurtleTest_TurtleTestGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	TurtleTest_Source_TurtleTest_TurtleTestGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	TurtleTest_Source_TurtleTest_TurtleTestGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TURTLETEST_API UClass* StaticClass<class ATurtleTestGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TurtleTest_Source_TurtleTest_TurtleTestGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
