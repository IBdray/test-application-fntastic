// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TurtleTest/PlayingCharacter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePlayingCharacter() {}
// Cross Module References
	TURTLETEST_API UClass* Z_Construct_UClass_APlayingCharacter_NoRegister();
	TURTLETEST_API UClass* Z_Construct_UClass_APlayingCharacter();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	UPackage* Z_Construct_UPackage__Script_TurtleTest();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	TURTLETEST_API UClass* Z_Construct_UClass_AInteractableActor_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(APlayingCharacter::execInteractWithTarget)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->InteractWithTarget();
		P_NATIVE_END;
	}
	static FName NAME_APlayingCharacter_OnFootstep = FName(TEXT("OnFootstep"));
	void APlayingCharacter::OnFootstep()
	{
		ProcessEvent(FindFunctionChecked(NAME_APlayingCharacter_OnFootstep),NULL);
	}
	static FName NAME_APlayingCharacter_OnInteractableFound = FName(TEXT("OnInteractableFound"));
	void APlayingCharacter::OnInteractableFound()
	{
		ProcessEvent(FindFunctionChecked(NAME_APlayingCharacter_OnInteractableFound),NULL);
	}
	static FName NAME_APlayingCharacter_OnInteractableLost = FName(TEXT("OnInteractableLost"));
	void APlayingCharacter::OnInteractableLost()
	{
		ProcessEvent(FindFunctionChecked(NAME_APlayingCharacter_OnInteractableLost),NULL);
	}
	void APlayingCharacter::StaticRegisterNativesAPlayingCharacter()
	{
		UClass* Class = APlayingCharacter::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "InteractWithTarget", &APlayingCharacter::execInteractWithTarget },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_APlayingCharacter_InteractWithTarget_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APlayingCharacter_InteractWithTarget_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "PlayingCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APlayingCharacter_InteractWithTarget_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APlayingCharacter, nullptr, "InteractWithTarget", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APlayingCharacter_InteractWithTarget_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APlayingCharacter_InteractWithTarget_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APlayingCharacter_InteractWithTarget()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APlayingCharacter_InteractWithTarget_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APlayingCharacter_OnFootstep_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APlayingCharacter_OnFootstep_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "PlayingCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APlayingCharacter_OnFootstep_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APlayingCharacter, nullptr, "OnFootstep", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APlayingCharacter_OnFootstep_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APlayingCharacter_OnFootstep_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APlayingCharacter_OnFootstep()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APlayingCharacter_OnFootstep_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APlayingCharacter_OnInteractableFound_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APlayingCharacter_OnInteractableFound_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "PlayingCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APlayingCharacter_OnInteractableFound_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APlayingCharacter, nullptr, "OnInteractableFound", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APlayingCharacter_OnInteractableFound_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APlayingCharacter_OnInteractableFound_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APlayingCharacter_OnInteractableFound()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APlayingCharacter_OnInteractableFound_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APlayingCharacter_OnInteractableLost_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APlayingCharacter_OnInteractableLost_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "PlayingCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APlayingCharacter_OnInteractableLost_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APlayingCharacter, nullptr, "OnInteractableLost", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APlayingCharacter_OnInteractableLost_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APlayingCharacter_OnInteractableLost_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APlayingCharacter_OnInteractableLost()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APlayingCharacter_OnInteractableLost_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_APlayingCharacter_NoRegister()
	{
		return APlayingCharacter::StaticClass();
	}
	struct Z_Construct_UClass_APlayingCharacter_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InteractionDetectionLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InteractionDetectionLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Scene_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Scene;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Camera_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Camera;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NonInteractableActors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NonInteractableActors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_NonInteractableActors;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InteractionTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InteractionTarget;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APlayingCharacter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_TurtleTest,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_APlayingCharacter_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_APlayingCharacter_InteractWithTarget, "InteractWithTarget" }, // 789327192
		{ &Z_Construct_UFunction_APlayingCharacter_OnFootstep, "OnFootstep" }, // 80460403
		{ &Z_Construct_UFunction_APlayingCharacter_OnInteractableFound, "OnInteractableFound" }, // 3160497985
		{ &Z_Construct_UFunction_APlayingCharacter_OnInteractableLost, "OnInteractableLost" }, // 3564912433
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayingCharacter_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "PlayingCharacter.h" },
		{ "ModuleRelativePath", "PlayingCharacter.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayingCharacter_Statics::NewProp_InteractionDetectionLength_MetaData[] = {
		{ "Category", "Interaction" },
		{ "ModuleRelativePath", "PlayingCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APlayingCharacter_Statics::NewProp_InteractionDetectionLength = { "InteractionDetectionLength", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlayingCharacter, InteractionDetectionLength), METADATA_PARAMS(Z_Construct_UClass_APlayingCharacter_Statics::NewProp_InteractionDetectionLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlayingCharacter_Statics::NewProp_InteractionDetectionLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayingCharacter_Statics::NewProp_Scene_MetaData[] = {
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "PlayingCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APlayingCharacter_Statics::NewProp_Scene = { "Scene", nullptr, (EPropertyFlags)0x002008000008001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlayingCharacter, Scene), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APlayingCharacter_Statics::NewProp_Scene_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlayingCharacter_Statics::NewProp_Scene_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayingCharacter_Statics::NewProp_Camera_MetaData[] = {
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "PlayingCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APlayingCharacter_Statics::NewProp_Camera = { "Camera", nullptr, (EPropertyFlags)0x002008000008001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlayingCharacter, Camera), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APlayingCharacter_Statics::NewProp_Camera_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlayingCharacter_Statics::NewProp_Camera_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APlayingCharacter_Statics::NewProp_NonInteractableActors_Inner = { "NonInteractableActors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayingCharacter_Statics::NewProp_NonInteractableActors_MetaData[] = {
		{ "Category", "Interaction" },
		{ "ModuleRelativePath", "PlayingCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_APlayingCharacter_Statics::NewProp_NonInteractableActors = { "NonInteractableActors", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlayingCharacter, NonInteractableActors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_APlayingCharacter_Statics::NewProp_NonInteractableActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlayingCharacter_Statics::NewProp_NonInteractableActors_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayingCharacter_Statics::NewProp_InteractionTarget_MetaData[] = {
		{ "Category", "Interaction" },
		{ "ModuleRelativePath", "PlayingCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APlayingCharacter_Statics::NewProp_InteractionTarget = { "InteractionTarget", nullptr, (EPropertyFlags)0x0020080000020015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APlayingCharacter, InteractionTarget), Z_Construct_UClass_AInteractableActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APlayingCharacter_Statics::NewProp_InteractionTarget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlayingCharacter_Statics::NewProp_InteractionTarget_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APlayingCharacter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayingCharacter_Statics::NewProp_InteractionDetectionLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayingCharacter_Statics::NewProp_Scene,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayingCharacter_Statics::NewProp_Camera,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayingCharacter_Statics::NewProp_NonInteractableActors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayingCharacter_Statics::NewProp_NonInteractableActors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayingCharacter_Statics::NewProp_InteractionTarget,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APlayingCharacter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APlayingCharacter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APlayingCharacter_Statics::ClassParams = {
		&APlayingCharacter::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_APlayingCharacter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_APlayingCharacter_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_APlayingCharacter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APlayingCharacter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APlayingCharacter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APlayingCharacter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APlayingCharacter, 1660795947);
	template<> TURTLETEST_API UClass* StaticClass<APlayingCharacter>()
	{
		return APlayingCharacter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APlayingCharacter(Z_Construct_UClass_APlayingCharacter, &APlayingCharacter::StaticClass, TEXT("/Script/TurtleTest"), TEXT("APlayingCharacter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APlayingCharacter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
