// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TURTLETEST_InteractableActor_generated_h
#error "InteractableActor.generated.h already included, missing '#pragma once' in InteractableActor.h"
#endif
#define TURTLETEST_InteractableActor_generated_h

#define TurtleTest_Source_TurtleTest_InteractableActor_h_14_SPARSE_DATA
#define TurtleTest_Source_TurtleTest_InteractableActor_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execIsActivated); \
	DECLARE_FUNCTION(execCanBeActivated);


#define TurtleTest_Source_TurtleTest_InteractableActor_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execIsActivated); \
	DECLARE_FUNCTION(execCanBeActivated);


#define TurtleTest_Source_TurtleTest_InteractableActor_h_14_EVENT_PARMS
#define TurtleTest_Source_TurtleTest_InteractableActor_h_14_CALLBACK_WRAPPERS
#define TurtleTest_Source_TurtleTest_InteractableActor_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAInteractableActor(); \
	friend struct Z_Construct_UClass_AInteractableActor_Statics; \
public: \
	DECLARE_CLASS(AInteractableActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TurtleTest"), NO_API) \
	DECLARE_SERIALIZER(AInteractableActor)


#define TurtleTest_Source_TurtleTest_InteractableActor_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAInteractableActor(); \
	friend struct Z_Construct_UClass_AInteractableActor_Statics; \
public: \
	DECLARE_CLASS(AInteractableActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TurtleTest"), NO_API) \
	DECLARE_SERIALIZER(AInteractableActor)


#define TurtleTest_Source_TurtleTest_InteractableActor_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AInteractableActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AInteractableActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AInteractableActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AInteractableActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AInteractableActor(AInteractableActor&&); \
	NO_API AInteractableActor(const AInteractableActor&); \
public:


#define TurtleTest_Source_TurtleTest_InteractableActor_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AInteractableActor(AInteractableActor&&); \
	NO_API AInteractableActor(const AInteractableActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AInteractableActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AInteractableActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AInteractableActor)


#define TurtleTest_Source_TurtleTest_InteractableActor_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Scene() { return STRUCT_OFFSET(AInteractableActor, Scene); } \
	FORCEINLINE static uint32 __PPO__bCanBeActivated() { return STRUCT_OFFSET(AInteractableActor, bCanBeActivated); } \
	FORCEINLINE static uint32 __PPO__bActivated() { return STRUCT_OFFSET(AInteractableActor, bActivated); }


#define TurtleTest_Source_TurtleTest_InteractableActor_h_11_PROLOG \
	TurtleTest_Source_TurtleTest_InteractableActor_h_14_EVENT_PARMS


#define TurtleTest_Source_TurtleTest_InteractableActor_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TurtleTest_Source_TurtleTest_InteractableActor_h_14_PRIVATE_PROPERTY_OFFSET \
	TurtleTest_Source_TurtleTest_InteractableActor_h_14_SPARSE_DATA \
	TurtleTest_Source_TurtleTest_InteractableActor_h_14_RPC_WRAPPERS \
	TurtleTest_Source_TurtleTest_InteractableActor_h_14_CALLBACK_WRAPPERS \
	TurtleTest_Source_TurtleTest_InteractableActor_h_14_INCLASS \
	TurtleTest_Source_TurtleTest_InteractableActor_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TurtleTest_Source_TurtleTest_InteractableActor_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TurtleTest_Source_TurtleTest_InteractableActor_h_14_PRIVATE_PROPERTY_OFFSET \
	TurtleTest_Source_TurtleTest_InteractableActor_h_14_SPARSE_DATA \
	TurtleTest_Source_TurtleTest_InteractableActor_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	TurtleTest_Source_TurtleTest_InteractableActor_h_14_CALLBACK_WRAPPERS \
	TurtleTest_Source_TurtleTest_InteractableActor_h_14_INCLASS_NO_PURE_DECLS \
	TurtleTest_Source_TurtleTest_InteractableActor_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TURTLETEST_API UClass* StaticClass<class AInteractableActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TurtleTest_Source_TurtleTest_InteractableActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
