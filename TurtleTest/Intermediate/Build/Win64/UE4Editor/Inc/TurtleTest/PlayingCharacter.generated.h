// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TURTLETEST_PlayingCharacter_generated_h
#error "PlayingCharacter.generated.h already included, missing '#pragma once' in PlayingCharacter.h"
#endif
#define TURTLETEST_PlayingCharacter_generated_h

#define TurtleTest_Source_TurtleTest_PlayingCharacter_h_16_SPARSE_DATA
#define TurtleTest_Source_TurtleTest_PlayingCharacter_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execInteractWithTarget);


#define TurtleTest_Source_TurtleTest_PlayingCharacter_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execInteractWithTarget);


#define TurtleTest_Source_TurtleTest_PlayingCharacter_h_16_EVENT_PARMS
#define TurtleTest_Source_TurtleTest_PlayingCharacter_h_16_CALLBACK_WRAPPERS
#define TurtleTest_Source_TurtleTest_PlayingCharacter_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlayingCharacter(); \
	friend struct Z_Construct_UClass_APlayingCharacter_Statics; \
public: \
	DECLARE_CLASS(APlayingCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TurtleTest"), NO_API) \
	DECLARE_SERIALIZER(APlayingCharacter)


#define TurtleTest_Source_TurtleTest_PlayingCharacter_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAPlayingCharacter(); \
	friend struct Z_Construct_UClass_APlayingCharacter_Statics; \
public: \
	DECLARE_CLASS(APlayingCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TurtleTest"), NO_API) \
	DECLARE_SERIALIZER(APlayingCharacter)


#define TurtleTest_Source_TurtleTest_PlayingCharacter_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlayingCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlayingCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayingCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayingCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayingCharacter(APlayingCharacter&&); \
	NO_API APlayingCharacter(const APlayingCharacter&); \
public:


#define TurtleTest_Source_TurtleTest_PlayingCharacter_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayingCharacter(APlayingCharacter&&); \
	NO_API APlayingCharacter(const APlayingCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayingCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayingCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APlayingCharacter)


#define TurtleTest_Source_TurtleTest_PlayingCharacter_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__InteractionDetectionLength() { return STRUCT_OFFSET(APlayingCharacter, InteractionDetectionLength); } \
	FORCEINLINE static uint32 __PPO__Scene() { return STRUCT_OFFSET(APlayingCharacter, Scene); } \
	FORCEINLINE static uint32 __PPO__Camera() { return STRUCT_OFFSET(APlayingCharacter, Camera); } \
	FORCEINLINE static uint32 __PPO__NonInteractableActors() { return STRUCT_OFFSET(APlayingCharacter, NonInteractableActors); } \
	FORCEINLINE static uint32 __PPO__InteractionTarget() { return STRUCT_OFFSET(APlayingCharacter, InteractionTarget); }


#define TurtleTest_Source_TurtleTest_PlayingCharacter_h_13_PROLOG \
	TurtleTest_Source_TurtleTest_PlayingCharacter_h_16_EVENT_PARMS


#define TurtleTest_Source_TurtleTest_PlayingCharacter_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TurtleTest_Source_TurtleTest_PlayingCharacter_h_16_PRIVATE_PROPERTY_OFFSET \
	TurtleTest_Source_TurtleTest_PlayingCharacter_h_16_SPARSE_DATA \
	TurtleTest_Source_TurtleTest_PlayingCharacter_h_16_RPC_WRAPPERS \
	TurtleTest_Source_TurtleTest_PlayingCharacter_h_16_CALLBACK_WRAPPERS \
	TurtleTest_Source_TurtleTest_PlayingCharacter_h_16_INCLASS \
	TurtleTest_Source_TurtleTest_PlayingCharacter_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TurtleTest_Source_TurtleTest_PlayingCharacter_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TurtleTest_Source_TurtleTest_PlayingCharacter_h_16_PRIVATE_PROPERTY_OFFSET \
	TurtleTest_Source_TurtleTest_PlayingCharacter_h_16_SPARSE_DATA \
	TurtleTest_Source_TurtleTest_PlayingCharacter_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	TurtleTest_Source_TurtleTest_PlayingCharacter_h_16_CALLBACK_WRAPPERS \
	TurtleTest_Source_TurtleTest_PlayingCharacter_h_16_INCLASS_NO_PURE_DECLS \
	TurtleTest_Source_TurtleTest_PlayingCharacter_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TURTLETEST_API UClass* StaticClass<class APlayingCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TurtleTest_Source_TurtleTest_PlayingCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
